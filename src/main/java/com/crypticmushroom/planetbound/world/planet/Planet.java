package com.crypticmushroom.planetbound.world.planet;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;

public abstract class Planet
{
    //private final ColorReloader grassColorReloader, foliageColorReloader;

    //public final ColorizerRonne grassColorizer, foliageColorizer;

    public Planet()
    {
        //this(new ColorizerRonne(new ResourceLocation(PBCore.MOD_ID, "textures/colormap/" + grassColormapName)), new ColorizerRonne(new ResourceLocation(PBCore.MOD_ID, "textures/colormap/" + foliageColormapName)));
    }

    /*
     * Use this if you know how to use it. I'm leaving this open just in case. ~Kino
     *
     * Sorry mate, a lot of it got chopped out. Don't believe some of those were needed. @Androsa
     */

    public abstract void generate(BlockPos chunkPos, Biome biome);

    public abstract String getName();

    @Override
    public final int hashCode()
    {
        return getName().hashCode();
    }
}