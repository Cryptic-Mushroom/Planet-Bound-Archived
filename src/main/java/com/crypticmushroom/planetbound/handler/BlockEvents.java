package com.crypticmushroom.planetbound.handler;

import com.crypticmushroom.planetbound.PBCore;
import com.crypticmushroom.planetbound.init.PBBlocks;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.item.ItemBlock;
import net.minecraft.world.biome.BiomeColorHelper;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
@Mod.EventBusSubscriber(modid = PBCore.MOD_ID, value = Side.CLIENT)
public class BlockEvents
{
    @SubscribeEvent
    public static void registerBlockColors(ColorHandlerEvent.Block e)
    {
        BlockColors blocks = e.getBlockColors();

        blocks.registerBlockColorHandler((state, worldIn, pos, tintIndex) -> worldIn != null && pos != null ? BiomeColorHelper.getGrassColorAtPos(worldIn, pos) : 0xF05F39,
                PBBlocks.ronnian_grass,
                PBBlocks.ronnian_tallgrass);
        blocks.registerBlockColorHandler((state, worldIn, pos, tintIndex) -> worldIn != null && pos != null ? BiomeColorHelper.getFoliageColorAtPos(worldIn, pos) : 0xFF3527,
                PBBlocks.emberwood_leaves,
                PBBlocks.amberwood_leaves);
    }

    @SubscribeEvent
    public static void registerItemColors(ColorHandlerEvent.Item e)
    {
        BlockColors blocks = e.getBlockColors();
        ItemColors items = e.getItemColors();

        items.registerItemColorHandler((stack, tintIndex) -> blocks.colorMultiplier(((ItemBlock)stack.getItem()).getBlock().getDefaultState(), null, null, tintIndex),
                PBBlocks.ronnian_grass,
                PBBlocks.ronnian_tallgrass,
                PBBlocks.emberwood_leaves,
                PBBlocks.amberwood_leaves);
    }
}